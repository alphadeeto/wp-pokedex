import { put, call } from 'redux-saga/effects';
import TypeActions from '../Redux/Type';

export function* loadTypeList(api, {meta}) {

  const { params, append, listName } = meta;
  try {
    const response = yield call(api.getTypeList, params);
    if (response.ok) {
      const data = response.data;
      yield put(TypeActions.typeFetchListSuccess({listName}));
      
      const pageData = {
        count: data.count,
      };

      if (data.next) {
        const next = new URL(data.next);
        pageData.offset = next.searchParams.get("offset");
      }
    
      const list = data.results.map(item => item.name);

      // Load the list
      if (append)
        yield put(TypeActions.typeAppendList(listName, list, pageData));
      else
        yield put(TypeActions.typeLoadList(listName, list, pageData));
    } else {
      yield put(TypeActions.typeFetchListFail({listName}, response.problem))
    } 
  } catch(e) {
    yield put(TypeActions.typeFetchListFail({listName}, e))
  }

}