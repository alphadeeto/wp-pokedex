import { expectSaga }    from "redux-saga-test-plan";
import { loadPokemonList, loadPokemonEntity } from '../Pokemon';
import PokemonActions from '../../Redux/Pokemon';

const listName = "test";
const id = "pokemon";

const successList = ["test1", "test2", "test3"];
const successPageData = {
    count: 3,
    offset: '0'
}
const successEntity = {
    name:"pokemon",
    ability:"do pokemon things"
}
const successApi = {
    getPokemonList: jest.fn(() => {
        return {
            ok: true,
            data: {
                results: [
                    {name: "test1"},
                    {name: "test2"},
                    {name: "test3"},
                ],
                next: "https://someurl.com/?offset=0",
                count: 3
            }
        }
    }),
    getPokemonEntity: jest.fn(() => {
        return {
            ok: true,
            data: {
                name:"pokemon",
                ability:"do pokemon things"
            }
        }
    })
};

const problem = "Server error"
const failApi = {
    getPokemonList: jest.fn(() => {
        return {
            ok: false,
            problem
        }
    }),
    getPokemonEntity: jest.fn(() => {
        return {
            ok: false,
            problem
        }
    })
}

test('LoadPokemonList without append should dispatch pokemonFetchListSuccess and pokemonLoadList', () => {
    const meta = {
        params: {},
        append: false,
        listName
    }

    return expectSaga(loadPokemonList, successApi, {meta})
        .call(successApi.getPokemonList, {})
        .put(PokemonActions.pokemonFetchListSuccess({listName}))
        .put(PokemonActions.pokemonLoadList(listName, successList, successPageData))
        .run();
});

test('LoadPokemonList with append should dispatch pokemonFetchListSuccess and pokemonAppendList', () => {
    const meta = {
        params: {},
        append: true,
        listName
    }

    return expectSaga(loadPokemonList, successApi, {meta})
        .call(successApi.getPokemonList, {})
        .put(PokemonActions.pokemonFetchListSuccess({listName}))
        .put(PokemonActions.pokemonAppendList(listName, successList, successPageData))
        .run();
});

test('LoadPokemonList api error should dispatch pokemonFetchListFail', () => {
    const meta = {
        params: {},
        append: false,
        listName
    }

    return expectSaga(loadPokemonList, failApi, {meta})
        .call(failApi.getPokemonList, {})
        .put(PokemonActions.pokemonFetchListFail({listName}, problem))
        .run();
});

test('LoadPokemonEntity should dispatch pokemonFetchEntitySuccess and pokemonLoadEntity', () => {
    return expectSaga(loadPokemonEntity, successApi, {id})
        .call(successApi.getPokemonEntity, id)
        .put(PokemonActions.pokemonFetchEntitySuccess())
        .put(PokemonActions.pokemonAddEntity(id, successEntity))
        .run();
});


test('LoadPokemonEntity api error should dispatch pokemonFetchEntityFail', () => {
    return expectSaga(loadPokemonEntity, failApi, {id})
        .call(failApi.getPokemonEntity, id)
        .put(PokemonActions.pokemonFetchEntityFail(problem))
        .run();
});