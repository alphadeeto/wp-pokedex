import { expectSaga }    from "redux-saga-test-plan";
import { loadTypeList } from '../Type';
import TypeActions from '../../Redux/Type';

const listName = "test";

const successList = ["test1", "test2", "test3"];
const successPageData = {
    count: 3,
    offset: '0'
}
const successApi = {
    getTypeList: jest.fn(() => {
        return {
            ok: true,
            data: {
                results: [
                    {name: "test1"},
                    {name: "test2"},
                    {name: "test3"},
                ],
                next: "https://someurl.com/?offset=0",
                count: 3
            }
        }
    }),
};

const problem = "Server error"
const failApi = {
    getTypeList: jest.fn(() => {
        return {
            ok: false,
            problem
        }
    }),
}

test('LoadTypeList without append should dispatch typeFetchListSuccess and typeLoadList', () => {
    const meta = {
        params: {},
        append: false,
        listName
    }

    return expectSaga(loadTypeList, successApi, {meta})
        .call(successApi.getTypeList, {})
        .put(TypeActions.typeFetchListSuccess({listName}))
        .put(TypeActions.typeLoadList(listName, successList, successPageData))
        .run();
});

test('LoadTypeList with append should dispatch typeFetchListSuccess and typeAppendList', () => {
    const meta = {
        params: {},
        append: true,
        listName
    }

    return expectSaga(loadTypeList, successApi, {meta})
        .call(successApi.getTypeList, {})
        .put(TypeActions.typeFetchListSuccess({listName}))
        .put(TypeActions.typeAppendList(listName, successList, successPageData))
        .run();
});

test('LoadTypeList api error should dispatch typeFetchListFail', () => {
    const meta = {
        params: {},
        append: false,
        listName
    }

    return expectSaga(loadTypeList, failApi, {meta})
        .call(failApi.getTypeList, {})
        .put(TypeActions.typeFetchListFail({listName}, problem))
        .run();
});