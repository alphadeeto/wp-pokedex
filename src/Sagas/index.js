import { takeLatest, all } from "redux-saga/effects";
import api from "../Services/Api";
 
/* ------------- Types -------------- */
import { PokemonTypes } from "../Redux/Pokemon";
import { TypeTypes } from "../Redux/Type";

/* ------------- Sagas -------------- */
import { loadPokemonList } from "./Pokemon"
import { loadPokemonEntity } from "./Pokemon"

import { loadTypeList } from "./Type"

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
// const api = DebugConfig.useFixtures ? FixtureAPI : API;

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
      takeLatest(PokemonTypes.POKEMON_FETCH_LIST_REQUEST, loadPokemonList, api),
      takeLatest(PokemonTypes.POKEMON_FETCH_ENTITY_REQUEST, loadPokemonEntity, api),

      takeLatest(TypeTypes.TYPE_FETCH_LIST_REQUEST, loadTypeList, api),
  ]);
}
