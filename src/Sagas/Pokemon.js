import { put, call } from 'redux-saga/effects';
import PokemonActions from '../Redux/Pokemon';

export function* loadPokemonList(api, {meta}) {

  let { params, append, listName } = meta;

  let byType = false;

  let fetchApi = api.getPokemonList

  // Check if fetch by type
  if (params.type) {
    byType = true;
    params = params.type;
    fetchApi = api.getPokemonListByType;
  }

  try {
    const response = yield call(fetchApi, params);
    if (response.ok) {
      const data = response.data;

      yield put(PokemonActions.pokemonFetchListSuccess({listName}));

      let pageData = {};
      let list = [];

      // If fetching by type, parse accordingly
      if (byType) {
        const pokemon = data.pokemon;

        pageData.count = pokemon.length;

        list = pokemon.map(item => item.pokemon.name)

      } else {
        pageData.count = data.count;
        if (data.next) {
          const next = new URL(data.next);
          pageData.offset = next.searchParams.get("offset");
        }

        list = data.results.map(item => item.name);
      }

      // Load the list
      if (append)
        yield put(PokemonActions.pokemonAppendList(listName, list, pageData));
      else
        yield put(PokemonActions.pokemonLoadList(listName, list, pageData));
    } else {
      yield put(PokemonActions.pokemonFetchListFail({listName}, response.problem))
    } 
  } catch(e) {
    yield put(PokemonActions.pokemonFetchListFail({listName}, e))
  }
}

export function* loadPokemonEntity(api, {id}) {

  try {
    const response = yield call(api.getPokemonEntity, id);
    if (response.ok) {
      const data = response.data;
      
      yield put(PokemonActions.pokemonFetchEntitySuccess());

      yield put(PokemonActions.pokemonAddEntity(id, data));
    } else {
      yield put(PokemonActions.pokemonFetchEntityFail(response.problem))
    } 
  } catch(e) {
    yield put(PokemonActions.pokemonFetchEntityFail(e))
  }

}