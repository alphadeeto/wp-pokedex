// a library to wrap and simplify api calls
import apisauce from "apisauce";

const create = (baseURL = "https://pokeapi.co/api/v2/") => {

  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      "Cache-Control": "no-cache"
    },
    // 10 second timeout...
    timeout: 10000
  });

  // API Endpoints
  const getPokemonList = params => api.get('pokemon', params);
  const getPokemonListByType = type => api.get(`type/${type}`);
  const getPokemonEntity = id => api.get(`pokemon/${id}`);
  const getTypeList = params => api.get('type', params);

  return {
    // a list of the API functions from step 2
    api,
    getPokemonList,
    getPokemonListByType,
    getPokemonEntity,
    getTypeList,
  };
};

// let's return back our create method as the default.
export default create();
