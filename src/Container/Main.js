import React, { Component } from 'react';
import { connect } from "react-redux";

import './styles/Main.css';

import List from '../Component/List';
import Detail from '../Component/Detail';
import Filter from '../Component/Filter';

import PokemonActions from '../Redux/Pokemon';
import TypeActions from '../Redux/Type';

class Main extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showDetail: false,
      itemId: 0,
      type: "All",
    }
  }

  componentDidMount() {
    this.props.fetchPokemonList({
      params: {},
      listName: "main",
      append: false,
    });
    
    this.props.fetchTypeList({
      params: {},
      listName: "main",
      append: false,
    });
  }

  render () {

    // Initialize vars
    let list = [];
    let detail = {};
    let filters = ["All"];
    let offset = 0;
    let listLoading = false;
    let detailLoading = false;
    let error = null;

    // Elements
    let listElement = null;
    let detailElement = null;
    let filterElement = null;

    // If pokemon list is not empty
    if (this.props.pokemon.main) {

      // Initialize props to be passed
      list = this.props.pokemon.main.data;
      offset = this.props.pokemon.main.offset;
      listLoading = this.props.pokemon.main.loading;
      error = this.props.pokemon.main.error;

      // Create element to hold list
      listElement = <List
        data={list}
        loading={listLoading}
        error={error}
        callBack={() => {
          if (this.state.type === "All") {
            this.props.fetchPokemonList({
              params: {offset},
              listName: "main",
              append: true
            })
          }
        }}
        onClick={(item) => {
          this.setState({showDetail:true, itemId: item});
          this.props.fetchPokemonEntity(item);
        }}
      />;
    }

    // If detail is shown, itemid is not null, and either it's loading or the pokemon detail is ready
    if (
      this.state.showDetail && this.state.itemId &&
      (this.props.pokemon.loading || this.props.pokemon.entities[this.state.itemId])) {
      detailLoading = this.props.pokemon.loading;
      detail = this.props.pokemon.entities[this.state.itemId];

      // Create element to hold detail
      detailElement = <Detail
        data={detail}
        loading={detailLoading}
        close={() => {
          this.setState({showDetail:false, itemId: 0});
        }}
        />
    }

    // If filters are not empty
    if (this.props.type.main) {
      filters = filters.concat(this.props.type.main.data);
    }

    filterElement = <Filter
      disabled={this.state.showDetail}
      data={filters}
      onChange={(item) => {
        let params = {}
        if (item !== "All") {
          params = {
            type: item
          }
        }
        this.setState({type: item})
        this.props.fetchPokemonList({
          params,
          listName: "main",
          append: false,
        });
      }}
      />
    
    return (
      <div className="main-container">
        <h1>
          Pokédex
        </h1>
        {filterElement}
        <div className="content">
          {this.state.showDetail && detailElement}
          {listElement}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    pokemon: state.pokemon,
    type: state.type
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchPokemonList: params => dispatch(PokemonActions.pokemonFetchListRequest(params)),
    fetchPokemonEntity: id => dispatch(PokemonActions.pokemonFetchEntityRequest(id)),
    fetchTypeList: params => dispatch(TypeActions.typeFetchListRequest(params)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
