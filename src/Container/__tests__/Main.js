import React from 'react';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import { render } from '@testing-library/react';
import Main from '../Main';

const mockStore = configureMockStore();
const store = mockStore({
  pokemon:{
    entities:{},
    loading:false,
    error:null,
    main:[]
  },
  type:{
    entities:{},
    loading:false,
    error:null,
    main:[]
  }
});

test('Main container renders correctly', () => {
  const { getByText } = render(
  <Provider store={store}>
    <Main />
  </Provider>);
  const pokedex = getByText(/Pokédex/i);
  expect(pokedex).toBeInTheDocument();
});
