import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Main from './Main';
import createStore from '../Redux';

// create our store
export const store = createStore();

class App extends Component {

  render () {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

// allow reactotron overlay for fast design in dev mode
export default App;