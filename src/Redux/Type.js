import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { 
  fetchListRequest,
  fetchListSuccess,
  fetchListFail,
  loadList,
  appendList,
} from './EntityReducer';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({

  // ================================================= FETCH LIST REQUEST TYPES ================================================= //   
  typeFetchListRequest: ['meta'],
  typeFetchListSuccess: ['meta'],
  typeFetchListFail: ['meta', 'payload'],

  // ================================================= ENTITIES AND LIST OPERATION TYPES ================================================= //   
  typeLoadList: ['listName', 'list', 'pageData'],
  typeAppendList: ['listName', 'list', 'pageData'],
});

export const TypeTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  entities: {},
  loading: false,
  error: null,
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  
  [Types.TYPE_FETCH_LIST_REQUEST]: fetchListRequest,
  [Types.TYPE_FETCH_LIST_SUCCESS]: fetchListSuccess,
  [Types.TYPE_FETCH_LIST_FAIL]: fetchListFail,
  [Types.TYPE_LOAD_LIST]: loadList,
  [Types.TYPE_APPEND_LIST]: appendList,
});