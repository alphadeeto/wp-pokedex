import {TypeTypes} from '../Type'
import TypeActions from '../Type'

test('There should be an action to fetch request', () => {
    const meta = {
        listName: "test",
    };
    const expectedAction = {
        type: TypeTypes.TYPE_FETCH_LIST_REQUEST,
        meta
    }
    expect(TypeActions.typeFetchListRequest(meta)).toEqual(expectedAction)
})

test('There should be an action for successful request', () => {
    const meta = {
        listName: "test",
    };
    const expectedAction = {
        type: TypeTypes.TYPE_FETCH_LIST_SUCCESS,
        meta
    }
    expect(TypeActions.typeFetchListSuccess(meta)).toEqual(expectedAction)
})

test('There should be an action for failed request', () => {
    const meta = {
        listName: "test",
    };
    const payload = {
        code: 500,
        message: "Server error"
    }
    const expectedAction = {
        type: TypeTypes.TYPE_FETCH_LIST_FAIL,
        meta,
        payload
    }
    expect(TypeActions.typeFetchListFail(meta, payload)).toEqual(expectedAction)
})

test('There should be an action for loading list', () => {
    const listName = "test";
    const list = [1, 2, 3];
    const pageData = {
        count: 3,
        offset: 0
    }
    const expectedAction = {
        type: TypeTypes.TYPE_LOAD_LIST,
        listName,
        list,
        pageData
    }
    expect(TypeActions.typeLoadList(listName, list, pageData)).toEqual(expectedAction)
})

test('Append list should append list under listname object with provided data', () => {
    const listName = "test";
    const list = [1, 2, 3];
    const pageData = {
        count: 3,
        offset: 0
    }
    const expectedAction = {
        type: TypeTypes.TYPE_APPEND_LIST,
        listName,
        list,
        pageData
    }
    expect(TypeActions.typeAppendList(listName, list, pageData)).toEqual(expectedAction)
})