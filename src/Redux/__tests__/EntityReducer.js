import {    
  fetchListRequest,
  fetchListSuccess,
  fetchListFail,
  fetchEntityRequest,
  fetchEntitySuccess,
  fetchEntityFail,
  loadEntities,
  addEntity,
  loadList,
  appendList,
} from '../EntityReducer';
import Immutable from 'seamless-immutable';

const state = Immutable({
    entities: {},
    loading: false,
    error: null,
});

test('Fetch list request should return the state with loading set as true and error set as null on list object', () => {
    const meta = {
        listName: "test",
    };
    expect(fetchListRequest(state, {meta})).toStrictEqual(Immutable({
        entities: {},
        loading: false,
        error: null,
        test: {
            data: [],
            count: 0,
            offset: 0,
            loading: true,
            error: null,
        }
    }))
})

test('Fetch list success should return the state with loading set as false and error set as null on list object', () => {
    const meta = {
        listName: "test",
    };
    expect(fetchListSuccess(state, {meta})).toStrictEqual(Immutable({
        entities: {},
        loading: false,
        error: null,
        test: {
            data: [],
            count: 0,
            offset: 0,
            loading: false,
            error: null,
        }
    }))
})

test('Fetch list fail should return the state with loading set as false and error set as received data on list object', () => {
    const meta = {
        listName: "test",
    };
    const payload = {
        code: 500,
        message: "Server error"
    }
    expect(fetchListFail(state, {meta, payload})).toStrictEqual(Immutable({
        entities: {},
        loading: false,
        error: null,
        test: {
            data: [],
            count: 0,
            offset: 0,
            loading: false,
            error: {
                code: 500,
                message: "Server error"
            },
        }
    }))
})

test('Fetch entity request should return the state with loading set as true and error set as null', () => {
    expect(fetchEntityRequest(state)).toStrictEqual(Immutable({
        entities: {},
        loading: true,
        error: null,
    }))
})

test('Fetch entity success should return the state with loading set as true and error set as null', () => {
    expect(fetchEntitySuccess(state)).toStrictEqual(Immutable({
        entities: {},
        loading: false,
        error: null,
    }))
})

test('Fetch entity fail should return the state with loading set as true and error set as null', () => {
    const payload = {
        code: 500,
        message: "Server error"
    }
    expect(fetchEntityFail(state, {payload})).toStrictEqual(Immutable({
        entities: {},
        loading: false,
        error: {
            code: 500,
            message: "Server error"
        },
    }))
})

test('Load entities should populate entities with provided data', () => {
    const entities = {
        1: {name: "test1"},
        2: {name: "test2"},
        3: {name: "test3"},
    }
    expect(loadEntities(state, {entities})).toStrictEqual(Immutable({
        entities: {
            1: {name: "test1"},
            2: {name: "test2"},
            3: {name: "test3"},
        },
        loading: false,
        error: null,
    }))
})

test('Add entity should add provided data into entities', () => {
    const id = 1;
    const data = {name: "test1"};
    expect(addEntity(state, {id, data})).toStrictEqual(Immutable({
        entities: {
            1: {name: "test1"}
        },
        loading: false,
        error: null,
    }))
})

test('Load list should populate list under listname object with provided data', () => {
    const listName = "test";
    const list = [1, 2, 3];
    const pageData = {
        count: 3,
        offset: 0
    }
    expect(loadList(state, {listName, list, pageData})).toStrictEqual(Immutable({
        entities: {},
        loading: false,
        error: null,
        test: {
            data: [1, 2, 3],
            count: 3,
            offset: 0,
            loading: false,
            error: null,
        }
    }))
})

test('Append list should append list under listname object with provided data', () => {
    const listName = "test";
    const list = [1, 2, 3];
    const pageData = {
        count: 3,
        offset: 0
    }
    expect(appendList(state, {listName, list, pageData})).toStrictEqual(Immutable({
        entities: {},
        loading: false,
        error: null,
        test: {
            data: [1, 2, 3],
            count: 3,
            offset: 0,
            loading: false,
            error: null,
        }
    }))
})