import {PokemonTypes} from '../Pokemon'
import PokemonActions from '../Pokemon'

test('There should be an action to fetch request', () => {
    const meta = {
        listName: "test",
    };
    const expectedAction = {
        type: PokemonTypes.POKEMON_FETCH_LIST_REQUEST,
        meta
    }
    expect(PokemonActions.pokemonFetchListRequest(meta)).toEqual(expectedAction)
})

test('There should be an action for successful request', () => {
    const meta = {
        listName: "test",
    };
    const expectedAction = {
        type: PokemonTypes.POKEMON_FETCH_LIST_SUCCESS,
        meta
    }
    expect(PokemonActions.pokemonFetchListSuccess(meta)).toEqual(expectedAction)
})

test('There should be an action for failed request', () => {
    const meta = {
        listName: "test",
    };
    const payload = {
        code: 500,
        message: "Server error"
    }
    const expectedAction = {
        type: PokemonTypes.POKEMON_FETCH_LIST_FAIL,
        meta,
        payload
    }
    expect(PokemonActions.pokemonFetchListFail(meta, payload)).toEqual(expectedAction)
})

test('There should be an action for loading entities', () => {
    const entities = {
        1: {name: "test1"},
        2: {name: "test2"},
        3: {name: "test3"},
    }
    const expectedAction = {
        type: PokemonTypes.POKEMON_LOAD_ENTITIES,
        entities
    }
    expect(PokemonActions.pokemonLoadEntities(entities)).toEqual(expectedAction)
})

test('There should be an action for adding entity', () => {
    const id = 1;
    const data = {name: "test1"};
    const expectedAction = {
        type: PokemonTypes.POKEMON_ADD_ENTITY,
        id,
        data
    }
    expect(PokemonActions.pokemonAddEntity(id, data)).toEqual(expectedAction)
})

test('There should be an action for loading list', () => {
    const listName = "test";
    const list = [1, 2, 3];
    const pageData = {
        count: 3,
        offset: 0
    }
    const expectedAction = {
        type: PokemonTypes.POKEMON_LOAD_LIST,
        listName,
        list,
        pageData
    }
    expect(PokemonActions.pokemonLoadList(listName, list, pageData)).toEqual(expectedAction)
})

test('Append list should append list under listname object with provided data', () => {
    const listName = "test";
    const list = [1, 2, 3];
    const pageData = {
        count: 3,
        offset: 0
    }
    const expectedAction = {
        type: PokemonTypes.POKEMON_APPEND_LIST,
        listName,
        list,
        pageData
    }
    expect(PokemonActions.pokemonAppendList(listName, list, pageData)).toEqual(expectedAction)
})