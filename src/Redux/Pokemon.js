import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { 
  fetchListRequest,
  fetchListSuccess,
  fetchListFail,
  fetchEntityRequest,
  fetchEntitySuccess,
  fetchEntityFail,
  loadEntities,
  addEntity,
  loadList,
  appendList,
} from './EntityReducer';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({

  // ================================================= FETCH LIST REQUEST TYPES ================================================= //   
  pokemonFetchListRequest: ['meta'],
  pokemonFetchListSuccess: ['meta'],
  pokemonFetchListFail: ['meta', 'payload'],

  // ================================================= FETCH ENTITY REQUEST TYPES ================================================= //   
  pokemonFetchEntityRequest: ['id'],
  pokemonFetchEntitySuccess: [],
  pokemonFetchEntityFail: ['payload'],

  // ================================================= ENTITIES AND LIST OPERATION TYPES ================================================= //   
  pokemonLoadEntities: ['entities'],
  pokemonAddEntity: ['id', 'data'],
  pokemonLoadList: ['listName', 'list', 'pageData'],
  pokemonAppendList: ['listName', 'list', 'pageData'],
});

export const PokemonTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  entities: {},
  loading: false,
  error: null,
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  
  [Types.POKEMON_FETCH_LIST_REQUEST]: fetchListRequest,
  [Types.POKEMON_FETCH_LIST_SUCCESS]: fetchListSuccess,
  [Types.POKEMON_FETCH_LIST_FAIL]: fetchListFail,
  
  [Types.POKEMON_FETCH_ENTITY_REQUEST]: fetchEntityRequest,
  [Types.POKEMON_FETCH_ENTITY_SUCCESS]: fetchEntitySuccess,
  [Types.POKEMON_FETCH_ENTITY_FAIL]: fetchEntityFail,
  
  [Types.POKEMON_LOAD_ENTITIES]: loadEntities,
  [Types.POKEMON_ADD_ENTITY]: addEntity,
  [Types.POKEMON_LOAD_LIST]: loadList,
  [Types.POKEMON_APPEND_LIST]: appendList,
});