import Immutable from 'seamless-immutable';

// This is a generic reducer that can be used to manipulate lists and entities

// Default list object template
const listDefault = {
  data: [],
  count: 0,
  offset: 0,
  loading: false,
  error: null,
};

export const INITIAL_STATE = Immutable({
  entities: {},
  loading: false,
  error: null,
});

// ================================================= FETCH LIST ================================================= // 

export const fetchListRequest = (state, {meta}) => {
  const { listName } = meta;
  const stateList = 
    state[listName] ?
    state[listName] : 
    Immutable(listDefault);
  return state.setIn([listName], stateList.merge({
    loading: true,
    error: null,
  }));
};

export const fetchListSuccess = (state, { meta }) => {
  const { listName } = meta;
  const stateList = 
    state[listName] ?
    state[listName] : 
    Immutable(listDefault);
  return state.set(listName,
    stateList.merge({
      loading: false,
      error: null,
    })
  );
};

export const fetchListFail = (state, {meta, payload}) => {
  const { listName } = meta;
  const stateList = 
    state[listName] ?
    state[listName] : 
    Immutable(listDefault);
  return state.set(listName,
    stateList.merge({
      loading:false,
      error:payload,
    })
  );
};

// ================================================= FETCH ENTITY ================================================= // 

export const fetchEntityRequest = (state) => {
  return state.merge({
    loading:true,
    error:null
  })
};

export const fetchEntitySuccess = (state) => {
  return state.merge({
    loading:false,
    error:null
  })
};

export const fetchEntityFail = (state, {payload}) => {
  return state.merge({
    loading:false,
    error:payload
  })
};

// ================================================= ENTITIES AND LIST OPS ================================================= // 

export const loadEntities = (state, {entities}) => {
  return state.set("entities", state.entities.merge(entities, {deep:true}));
};

export const addEntity = (state, {id, data}) => {
  return state.setIn(["entities", id], data);
}

export const loadList = (state, {listName, list, pageData}) => {
  return state
    .setIn([listName],
      Immutable(pageData)
        .set("loading", false)
        .set("error", null)
        .set("data", list)
      )
};

export const appendList = (state, {listName, list, pageData}) => {
  const stateList = 
    state[listName] ?
    state[listName] : 
    Immutable(listDefault);
  return state
  .setIn([listName], Immutable(pageData)
    .set("loading", false)
    .set("error", null)
    .set("data", stateList.data.concat(list)));
};
