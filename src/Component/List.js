import React, {
  Component
} from 'react';
import ListItem from './ListItem';
import PropTypes from 'prop-types';
import './styles/List.css'

export default class List extends Component {
  static propTypes = {
    data: PropTypes.array,
    callBack: PropTypes.func,
    loading: PropTypes.bool,
    error: PropTypes.string,
    onClick: PropTypes.func,
  }
  
  // Handle scroll event, run callback if reached bottom
  handleScroll = (e) => {
    const bottom = e.target.scrollHeight - e.target.scrollTop - 100 < e.target.clientHeight;
    if (bottom) this.props.callBack()
  }

  render() {
    return (
      <ul 
        onScroll={this.handleScroll}
        className='list'>
        {(this.props.data || []).map((item, idx) =>
          <ListItem
            key={idx}
            data={item}
            onClick={(item) => {
              this.props.onClick && this.props.onClick(item);
            }}/>
        )}
        {this.props.loading && <li className='list-item loading'>Loading</li>}
        {this.props.error && <li className='list-item error'>{this.props.error}</li>}
      </ul>
    );
  }
}