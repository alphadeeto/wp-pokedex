import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles/Filter.css'

export default class Filter extends Component {
  static defaultProps = {}

  static propTypes = {
    data: PropTypes.array,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
  }

  render () {
    return (
      <div className="filter">
        <span className="label">Type</span>
        <select
          disabled={this.props.disabled}
          onChange={(e) => {
            this.props.onChange(e.target.value)
          }}>
          {(this.props.data || []).map((item) => (
            <option value={item}>{item}</option>
          ))}
        </select>
      </div>
    );
  }
}
