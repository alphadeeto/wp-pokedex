import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import List from '../List';

const data = [
  "item1",
  "item2",
  "item3",
  "item4",
  "item5",
  "item6",
];

const emptyData = [];
const error = "Server error";

const callback = jest.fn();

test('List should be empty if no data passed', () => {
  const { container } = render(<List data={emptyData} />);
  expect(container.firstChild).toBeEmpty()
});

test('List should display information on loading', () => {
  const { getByText } = render(<List loading={true} />);
  const loadingElement = getByText("Loading");
  expect(loadingElement).toBeInTheDocument();
});

test('List should display information if there is an error', () => {
  const { getByText } = render(<List error={error} />);
  const loadingElement = getByText(error);
  expect(loadingElement).toBeInTheDocument();
});

test('List should display item names passed on data', () => {
  const { getByText } = render(<List data={data} />);
  data.forEach(item => {
    const itemElement = getByText(item);
    expect(itemElement).toBeInTheDocument();
  })
});

test('Callback function should be triggered on scrolling to end', () => {
  const { container } = render(<List data-testid={"test"} data={data} callBack={callback} />);
  fireEvent.scroll(container.firstChild);
  expect(callback).toHaveBeenCalledTimes(1);
});