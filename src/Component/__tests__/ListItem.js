import React from 'react';
import { render } from '@testing-library/react';
import ListItem from '../ListItem';

const data = "Item name"

test('Item name should be rendered within list item', () => {
  const { getByText } = render(<ListItem data={data} />);
  const nameElement = getByText(data);
  expect(nameElement).toBeInTheDocument();
});
