import React, {
    Component
  } from 'react';
  import PropTypes from 'prop-types';
  import './styles/Detail.css'
  
  export default class List extends Component {
    static propTypes = {
      data: PropTypes.object,
      close: PropTypes.func,
      loading: PropTypes.bool,
    }
  
    render() {
      const detail = this.props.data;
      let detailElement;
      
      // If detail data is not empty
      if (detail) {

        // Create element to hold detail data
        detailElement = (
          <div>
            {/* ============ HEADER ============ */}
            <div className="header">
              <img
                className="sprites"
                src={detail.sprites.front_default} alt={detail.name}        
                data-testid="sprite-img"
                />
              <div className="header-info">
                <div className="field">
                  <div className="label">#{detail.id}</div>
                </div>
                <h2>{detail.name}</h2>
                <div className="field">
                  <div className="label">Base exp</div>
                  <div className="value">{detail.base_experience}</div>
                </div>
                <div className="field">
                  <div className="label">Weight</div>
                  <div className="value">{detail.weight}</div>
                </div>
                <div className="field">
                  <div className="label">Height</div>
                  <div className="value">{detail.height}</div>
                </div>
              </div>
            </div>
  
            {/* ============ SECTIONS ============ */}
            <div className="section-container">
  
              <div className="left-section">

                <div className="section">
                  <h3>Stats</h3>
                  {detail.stats.map((item, idx) => (
                    <div className="field" key={idx}>
                      <div className="label">{item.stat.name}</div>
                      <div className="value">{item.base_stat}</div>
                    </div>
                  ))}
                </div>
  
                <div className="section">
                  <h3>Types</h3>
                  {detail.types.map((item, idx) => (
                    <div className="field" key={idx}>
                      <div className="label">{item.type.name}</div>
                    </div>
                  ))}
                </div>
  
                <div className="section">
                  <h3>Abilities</h3>
                  {detail.abilities.map((item, idx) => (
                    <div className="field" key={idx}>
                      <div className="label">{item.ability.name}</div>
                    </div>
                  ))}
                </div>
              </div>
  
              <div className="right-section">
                <div className="section moves">
                  <h3>Moves</h3>
                  <div className="moves-container">
                    {detail.moves.map((item, idx) => (
                      <div className="field" key={idx}>
                        <div className="label">{item.move.name}</div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            
            </div>
  
          </div>
        );
      } 
      
      // Create element for loading
      const loadingElement = <div className="loading">LOADING</div>

      return (
        <div className="overlay">
          <div className="detail">
            <div className="close-btn" onClick={this.props.close}>CLOSE</div>
            {this.props.loading ? loadingElement : detailElement}
          </div>
        </div>
      );
    }
  }