import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles/ListItem.css'

export default class ListItem extends Component {
  static defaultProps = {}

  static propTypes = {
    data: PropTypes.string,
    onClick: PropTypes.func,
  }

  render () {
    return <li
      className='list-item'
      onClick={() => {
        this.props.onClick && this.props.onClick(this.props.data)
      }}
      >
      {this.props.data}
    </li>;
  }
}
